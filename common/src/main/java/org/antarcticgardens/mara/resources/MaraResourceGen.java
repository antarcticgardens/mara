package org.antarcticgardens.mara.resources;


import org.antarcticgardens.mara.blocks.MaraBlocks;
import org.antarcticgardens.mara.resources.utils.EnUsLanguage;

import java.io.File;

import static org.antarcticgardens.mara.Mara.MOD_ID;

public class MaraResourceGen {

    public static File root = new File("./common/src/main/resources");

    public static void main(String[] args) {

        EnUsLanguage.translations.put("itemGroup." + MOD_ID + ".tab", "Mara");

        for (MaraBlocks.MBlock block : MaraBlocks.blocks) {
            if (block.enUs != null) {
                EnUsLanguage.addBlock(block.id, block.enUs);
            }
            if (block.model != null) {
                block.model.get().run(block.id);
            }
        }

        EnUsLanguage.finish();
    }

}

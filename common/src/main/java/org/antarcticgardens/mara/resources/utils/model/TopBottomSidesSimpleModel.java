package org.antarcticgardens.mara.resources.utils.model;

import org.antarcticgardens.mara.resources.MaraResourceGen;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import static org.antarcticgardens.mara.Mara.MOD_ID;

public class TopBottomSidesSimpleModel extends BlockModel {
    public String top, side, bottom;
    public TopBottomSidesSimpleModel(String top, String side, String bottom) {
        this.top = top;
        this.side = side;
        this.bottom = bottom;
    }

    @Override
    public void run(String blockId) {
        String model = String.format("""
                {
                  "parent": "minecraft:block/cube_bottom_top",
                  "textures": {
                    "bottom": "%s",
                    "particle": "%s",
                    "side": "%s",
                    "top": "%s"
                  }
                }
                """, bottom, side, side, top);

        String blockstate = String.format("""
                {
                  "variants": {
                    "": {
                      "model": "%s:block/%s"
                    }
                  }
                }
                """, MOD_ID, blockId);

        String item = String.format("""
                {
                  "parent": "%s:block/%s"
                }
                """, MOD_ID, blockId);

        try {
            var modelOut = new File(MaraResourceGen.root,"assets/" + MOD_ID + "/models/block/" + blockId + ".json");
            modelOut.getParentFile().mkdirs();
            Files.write(modelOut.toPath(), model.getBytes(), StandardOpenOption.CREATE);
            var itemOut = new File(MaraResourceGen.root, "assets/" + MOD_ID + "/models/item/" + blockId + ".json");
            itemOut.getParentFile().mkdirs();
            Files.write(itemOut.toPath(), item.getBytes(), StandardOpenOption.CREATE);
            File stateOut = new File(MaraResourceGen.root, "assets/" + MOD_ID + "/blockstates/" + blockId + ".json");
            stateOut.getParentFile().mkdirs();
            Files.write(stateOut.toPath(), blockstate.getBytes(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}

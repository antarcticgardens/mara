package org.antarcticgardens.mara.resources.utils;

import org.antarcticgardens.mara.Mara;
import org.antarcticgardens.mara.resources.MaraResourceGen;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

public class EnUsLanguage {

    public static Map<String, String> translations = new HashMap<>();

    public static void addBlock(String id, String name) {
        translations.put(String.format("block.%s.%s", Mara.MOD_ID, id), name);
    }

    public static void finish() {
        StringBuilder content = new StringBuilder("{\n");
        for (Map.Entry<String, String> translation : translations.entrySet()) {
            content.append("\t\"").append(translation.getKey()).append("\": \"").append(translation.getValue()).append("\",\n");
        }
        content.replace(content.length() - 2, content.length()-1, "");
        content.append("}");
        try {
            File f = new File(MaraResourceGen.root, "assets/" + Mara.MOD_ID + "/lang/en_us.json");
            f.getParentFile().mkdirs();

            Files.write(f.toPath(), content.toString().getBytes(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

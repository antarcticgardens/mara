package org.antarcticgardens.mara.generation.features;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;

public class FeatureSideTree extends Feature<FeatureSideTree.FeatureSideTreeConfig> {

    public FeatureSideTree() {
        super(FeatureSideTreeConfig.CODEC);
    }

    @Override
    public boolean place(FeaturePlaceContext<FeatureSideTreeConfig> context) {
        Direction[] sides = new Direction[] {Direction.NORTH, Direction.SOUTH, Direction.EAST, Direction.WEST};
        var cfg = context.config();
        var level = context.level();
        var origin = context.origin();
        if (level.getBlockState(origin).is(cfg.wall)) {
            remover: {
                for (Direction direction : sides) {
                    if (level.isEmptyBlock(origin.relative(direction))) {
                        break remover;
                    }
                }
                return false;
            }
        } else if (!level.isEmptyBlock(origin)) {
            return false;
        }

        direction_control: {
            for (Direction direction : sides) {
                var block = context.origin();
                for (int i = 0; i < 8; i++) {
                    block = block.relative(direction);
                    if (level.getBlockState(block).is(cfg.wall)) {
                        origin = block;
                        break direction_control;
                    } else if (!level.isEmptyBlock(block)) {
                        return false;
                    }
                }
            }
            return false;
        }

        if (level.isEmptyBlock(origin.relative(Direction.UP)) || level.isEmptyBlock(origin.relative(Direction.DOWN))) {
            return false;
        }

        level.setBlock(origin, cfg.log.getState(context.random(), origin), 19);

        var square = cfg.radius * cfg.radius;
        for (int x = -cfg.radius; x < cfg.radius; x++) {
            for (int y = -cfg.radius; y < cfg.radius; y++) {
                if (x*x+y*y <= square) {
                    var pos = origin.offset(x, 0, y);
                    if (level.isEmptyBlock(pos)) {
                        level.setBlock(pos, cfg.leaves.getState(context.random(), origin), 19);
                    }
                }
            }
        }

        return true;
    }

    public record FeatureSideTreeConfig(int radius, BlockStateProvider leaves, BlockStateProvider log, TagKey<Block> wall) implements FeatureConfiguration {
        public static final Codec<FeatureSideTreeConfig> CODEC = RecordCodecBuilder.create(
                instance -> instance.group(
                                Codec.INT.fieldOf("radius").forGetter(FeatureSideTreeConfig::radius),
                                BlockStateProvider.CODEC.fieldOf("leaves").forGetter(FeatureSideTreeConfig::leaves),
                                BlockStateProvider.CODEC.fieldOf("log").forGetter(FeatureSideTreeConfig::log),
                                TagKey.codec(Registries.BLOCK).fieldOf("wall").forGetter(FeatureSideTreeConfig::wall))
                        .apply(instance, FeatureSideTreeConfig::new));
    }

}

package org.antarcticgardens.mara.generation;

import net.minecraft.world.level.levelgen.feature.Feature;
import org.antarcticgardens.mara.generation.features.FeatureCanyonVine;
import org.antarcticgardens.mara.generation.features.FeatureFloatingOrb;
import org.antarcticgardens.mara.generation.features.FeatureSideTree;

import java.util.Map;
import java.util.function.Supplier;

public class MaraFeatures {

    public static Map<String, Supplier<Feature<?>>> features = Map.of(
           "side_tree", FeatureSideTree::new,
            "canyon_vine", FeatureCanyonVine::new,
            "floating_orb", FeatureFloatingOrb::new
    );
}

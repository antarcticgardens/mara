package org.antarcticgardens.mara.generation.features;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import org.joml.Math;

public class FeatureFloatingOrb extends Feature<FeatureFloatingOrb.FeatureConfig> {

    public FeatureFloatingOrb() {
        super(FeatureConfig.CODEC);
    }

    @Override
    public boolean place(FeaturePlaceContext<FeatureConfig> context) {
        var cfg = context.config();
        var level = context.level();
        var origin = context.origin();

        if (level.isEmptyBlock(origin)) {
            level.setBlock(origin, cfg.block.getState(context.random(), origin), 0);
            return true;
        }

        return false;
    }

    public record FeatureConfig(BlockStateProvider block) implements FeatureConfiguration {
        public static final Codec<FeatureConfig> CODEC = RecordCodecBuilder.create(
                instance -> instance.group(
                                BlockStateProvider.CODEC.fieldOf("block").forGetter(FeatureConfig::block))
                        .apply(instance, FeatureConfig::new));
    }

}

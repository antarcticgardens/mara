package org.antarcticgardens.mara.generation.features;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import org.joml.Math;

public class FeatureCanyonVine extends Feature<FeatureCanyonVine.FeatureConfig> {

    public FeatureCanyonVine() {
        super(FeatureConfig.CODEC);
    }

    @Override
    public boolean place(FeaturePlaceContext<FeatureConfig> context) {
        var cfg = context.config();
        var level = context.level();
        var origin = context.origin();

        if (!level.isEmptyBlock(origin)) {
            return false;
        }

        float direction = (float) (context.random().nextFloat() * Math.PI);
        float dx = Math.sin(direction);
        float dz = Math.cos(direction);

        BlockPos start;
        check: {
            for (int i = 0; i < 16; i++) {
                var block = origin.offset((int) (dx * i), 0, (int) (dz * i));
                if (level.getBlockState(block).is(cfg.wall)) {
                    start = block;
                    break check;
                }
            }
            return false;
        }

        BlockPos end;
        check: {
            for (int i = 0; i < 16; i++) {
                var block = origin.offset((int) -(dx * i), 0, (int) -(dz * i));
                if (level.getBlockState(block).is(cfg.wall)) {
                    end = block;
                    break check;
                }
            }
            return false;
        }

        int len;
        if (Math.abs(start.getX() - end.getX()) > Math.abs(start.getZ() - end.getZ())) {
            len = Math.abs(start.getX() - end.getX());
        } else {
            len = Math.abs(start.getZ() - end.getZ());
        }

        var attempt = start.offset(0, context.random().nextIntBetweenInclusive(-len/5, len/5), 0);
        if (level.getBlockState(attempt).is(cfg.wall)) {
            start = attempt;
        }

        attempt = end.offset(0, context.random().nextIntBetweenInclusive(-len/5, len/5), 0);
        if (level.getBlockState(attempt).is(cfg.wall)) {
            end = attempt;
        }

        for (float i = -0.2f; i < 1.2f; i+= 0.8f/len) {
            var pos = start.offset(
                    (int) ((end.getX() - start.getX()) * (i)),
                    (int) ((end.getY() - start.getY()) * (i) - (1/(Math.abs(i-0.45)+1)) * len * 0.5),
                    (int) ((end.getZ() - start.getZ()) * (i))
            );
            placeLeaves(context, level, pos, cfg);
            for (Direction di : Direction.values()) {
                placeLeaves(context, level, pos.relative(di), cfg);
            }
        }

        return true;
    }

    private static void placeLeaves(FeaturePlaceContext<FeatureConfig> context, WorldGenLevel level, BlockPos pos, FeatureConfig cfg) {
        if (!level.isEmptyBlock(pos)) {
            return;
        }
        level.setBlock(
                pos,
                cfg.leaves.getState(context.random(), pos), 0
        );
    }

    public record FeatureConfig(BlockStateProvider leaves, TagKey<Block> wall) implements FeatureConfiguration {
        public static final Codec<FeatureConfig> CODEC = RecordCodecBuilder.create(
                instance -> instance.group(
                                BlockStateProvider.CODEC.fieldOf("leaves").forGetter(FeatureConfig::leaves),
                                TagKey.codec(Registries.BLOCK).fieldOf("wall").forGetter(FeatureConfig::wall))
                        .apply(instance, FeatureConfig::new));
    }

}

package org.antarcticgardens.mara.blocks.functionalterrain;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import org.antarcticgardens.mara.blocks.MaraBlocks;
import org.antarcticgardens.mara.resources.utils.model.TopBottomSidesSimpleModel;

public class WaterGeyser extends Block {

    public static WaterGeyser BLOCK;

    public WaterGeyser() {
        super(Properties.ofFullCopy(Blocks.STONE));
        BLOCK = this;
    }

    public static MaraBlocks.MBlock mBlock() {
        return new MaraBlocks.MBlock("water_geyser", WaterGeyser::new)
                .enUs("Water Geyser")
                .model(() -> new TopBottomSidesSimpleModel("mara:block/functional_terrain/water_geyser_top",
                        "minecraft:block/stone",
                        "minecraft:block/stone"));
    }
}

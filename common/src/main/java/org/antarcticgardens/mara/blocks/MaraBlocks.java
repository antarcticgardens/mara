package org.antarcticgardens.mara.blocks;

import net.minecraft.world.level.block.Block;
import org.antarcticgardens.mara.blocks.functionalterrain.WaterGeyser;
import org.antarcticgardens.mara.resources.utils.model.BlockModel;

import java.util.List;
import java.util.function.Supplier;

public class MaraBlocks {

    public static List<MBlock> blocks = List.of(
            WaterGeyser.mBlock()
    );

    public static class MBlock {
        public String id;
        public Supplier<Block> block;
        public String enUs;
        public boolean item = true;
        public Supplier<BlockModel> model;
        public MBlock(String id, Supplier<Block> block) {
            this.id = id;
            this.block = block;
        }

        public MBlock enUs(String enUs) {
            this.enUs = enUs;
            return this;
        }

        public MBlock model(Supplier<BlockModel> model) {
            this.model = model;
            return this;
        }

    }

}

package org.antarcticgardens.mara;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;
import org.antarcticgardens.mara.blocks.MaraBlocks;
import org.antarcticgardens.mara.blocks.functionalterrain.WaterGeyser;
import org.antarcticgardens.mara.generation.MaraFeatures;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static org.antarcticgardens.mara.Mara.MOD_ID;

@Mod("mara")
public class MaraForge {

    public MaraForge(IEventBus modBus) {

        Mara.logger = LoggerFactory.getLogger("Mara");

        Mara.logger.info("Time to add some dreams!");

        final DeferredRegister<Feature<?>> features = DeferredRegister.create(BuiltInRegistries.FEATURE, "mara");
        features.register(modBus);
        for (Map.Entry<String, Supplier<Feature<?>>> feature : MaraFeatures.features.entrySet()) {
            features.register(feature.getKey(), feature.getValue());
        }

        List<DeferredHolder<Item, BlockItem>> tabItems = new ArrayList<>();

        final DeferredRegister<Item> items = DeferredRegister.create(BuiltInRegistries.ITEM, "mara");
        items.register(modBus);

        final DeferredRegister<Block> blocks = DeferredRegister.create(BuiltInRegistries.BLOCK, "mara");
        blocks.register(modBus);
        for (MaraBlocks.MBlock block : MaraBlocks.blocks) {
            var b = blocks.register(block.id, block.block);
            if (block.item) {
                var item = items.register(block.id, () -> new BlockItem(b.get(), new Item.Properties()));
                tabItems.add(item);
            }
        }

        final var tabs = DeferredRegister.create(BuiltInRegistries.CREATIVE_MODE_TAB, "mara");
        tabs.register(modBus);
        var tab = tabs.register("mara", () -> CreativeModeTab.builder()
                .title(Component.translatable("itemGroup." + MOD_ID + ".tab"))
                .icon(() -> new ItemStack(WaterGeyser.BLOCK))
                .displayItems((params, output) -> {
                    for (DeferredHolder<Item, BlockItem> tabItem : tabItems) {
                        output.accept(tabItem.get());
                    }
                })
                .build()
        );

    }



}
